using Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;

namespace Splitter.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSpecTwo()
        {
            var configuration = new SplitterConfiguration()
            {
                MaximumTweetLength = 40
            };

            var splitter = new Core.Splitter(configuration);

            var inputMessage =
                "Without requirements or design, programming is the art of adding bugs to an empty text file.";

            var expected = new List<string>
            {
                "[1/4]  Without requirements or design, >",
                "[2/4]  < programming is the art of >",
                "[3/4]  < adding bugs to an empty text >",
                "[4/4]  < file."
            };

            var actual = splitter.Split(inputMessage).Select(x => x.Message).ToList();

            CollectionAssert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestSpecThree()
        {
            var configuration = new SplitterConfiguration()
            {
                MaximumTweetLength = 45
            };

            var splitter = new Core.Splitter(configuration);

            var inputMessage =
                "@user1 You will need to speak to @user2 about this";

            var expected = new List<string>
            {
                "[1/2] @user1 @user2 You will need to speak >",
                "[2/2] @user1 @user2 < to @user2 about this"
            };

            var actual = splitter.Split(inputMessage).Select(x => x.Message).ToList();

            CollectionAssert.AreEqual(expected, actual);
        }
    }
}
