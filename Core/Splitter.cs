﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace Core
{
    /// <summary>
    /// Splits a single long message into a series of Tweets 
    /// </summary>
    public class Splitter
    {
        private readonly SplitterConfiguration splitterConfiguration;

        /// <summary>
        /// Initializes a new instance of the <see cref="Splitter"/> class.
        /// </summary>
        /// <param name="splitterConfiguration">The splitter configuration.</param>
        /// <exception cref="NotImplementedException"></exception>
        public Splitter(SplitterConfiguration splitterConfiguration)
        {
            this.splitterConfiguration = splitterConfiguration;
        }

        /// <summary>
        /// Splits the specified message into a series of tweets.
        /// </summary>
        /// <param name="message">The message to be split.</param>
        /// <returns>A series of tweets to be posted to Twitter, in the order in which they should be posted.</returns>
        /// <exception cref="NotImplementedException"></exception>
        public IEnumerable<ITweet> Split(string message)
        {
            var mentions = Regex.Matches(message, @"@\w+");

            var mentionString = string.Empty;
            foreach(Match mention in mentions)
            {
                mentionString += $"{mention.Value}" + " ";

                if(mention.Index == 0)
                {
                    message = message.Remove(0, mention.Length);
                }
            }

            mentionString =  mentionString.Trim();

            const int numberOfFormattingChars = 9;
            int maxTweetLength;

            if (mentionString.Length > 0)
            {
                maxTweetLength =
                     splitterConfiguration.MaximumTweetLength - numberOfFormattingChars - mentionString.Length + 1;
            }
            else
            {
                maxTweetLength =
                    splitterConfiguration.MaximumTweetLength - numberOfFormattingChars;
            }

            var messages = new List<string>();
            var atEndOfMessage = false;
            var currentIndex = 0;

            while (!atEndOfMessage)
            {
                var remainingText = message.Substring(currentIndex, maxTweetLength + 1);

                var nextPunctuationIndex = remainingText.LastIndexOfAny(new char[] { '.', ',', '!', '?' });

                // No punctuation, need to look for whitespace instead
                if (nextPunctuationIndex == -1)
                {
                    var nextSpaceIndex = remainingText.LastIndexOfAny(new char[] { ' ' });
                    messages.Add(message.Substring(currentIndex, nextSpaceIndex).Trim());
                    currentIndex += nextSpaceIndex;
                }
                // Found some punctuation
                else
                {
                    messages.Add(message.Substring(currentIndex, nextPunctuationIndex + 2).Trim());
                    currentIndex += nextPunctuationIndex + 2;
                }

                //We're at the end of the message
                if (message.Length - currentIndex < maxTweetLength)
                {
                    messages.Add(message.Substring(currentIndex, message.Length - currentIndex).Trim());
                    atEndOfMessage = true;
                }

            }

            var tweetIndex = 1;
            var tweets = new List<ITweet>();

            foreach (var msg in messages)
            {
                var continuation = splitterConfiguration.ContinuationText;

                if (tweetIndex == 1)
                {
                    continuation = string.Empty;
                }

                var continues = splitterConfiguration.ContinuesText;

                if (tweetIndex == messages.Count)
                {
                    continues = string.Empty;
                }

                tweets.Add(new Tweet($"[{tweetIndex}/{messages.Count}] {mentionString} {continuation}{msg}{continues}"));
                tweetIndex++;
            }

            return tweets;
        }
    }
}
