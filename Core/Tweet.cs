﻿namespace Core
{
    internal sealed class Tweet : ITweet
    {
        public Tweet(string message)
        {
            this.Message = message;
        }

        public string Message { get; set; }
    }
}